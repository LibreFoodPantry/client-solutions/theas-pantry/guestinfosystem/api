# Automated Testing

We use scripts to call other scripts that run the tests.

```bash
commands/test.sh
```

This runs `commands/test.sh` which will validate the OpenAPI specification
using [Swagger/OpenAPI CLI](https://www.npmjs.com/package/swagger-cli).
