---
openapi: 3.0.3
servers:
  - url: 'http://localhost'
info:
  title: GuestInfoSystemAPI
  description: Guest information for Thea's Pantry
  version: 0.0.0
  contact:
    name: Thea's Pantry
    email: info@librefoodpantry.org
    url: librefoodpantry.org
tags:
  - name: guests
    description: Guests to Thea's Pantry
  - name: version
    description: API version
paths:
  /guests:
    description: All guests
    get:
      description: Gets all guests data.
      tags:
        - guests
      operationId: listGuests
      x-eov-operation-handler: endpoints
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/GuestArray'
        '400':
          $ref: '#/components/responses/BadRequest'
        '500':
          $ref: '#/components/responses/ServerError'
    post:
      description: Adds a new guest's data.
      tags:
        - guests
      operationId: createGuest
      x-eov-operation-handler: endpoints
      requestBody:
        description: Guest data
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Guest'
      responses:
        '201':
          description: Created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Guest'
          headers:
            Location:
              description: URI of new item.
              explode: false
              required: true
              schema:
                minLength: 1
                nullable: false
                type: string
              style: simple
        '400':
          $ref: '#/components/responses/BadRequest'
        '409':
          $ref: '#/components/responses/Conflict'
        '500':
          $ref: '#/components/responses/ServerError'
  '/guests/{wsuID}':
    description: Guest with given id.
    parameters:
      - name: wsuID
        in: path
        required: true
        description: "Worcester State University ID of the guest whose info
          is desired."
        schema:
          $ref: '#/components/schemas/WSUID'
    get:
      description: Gets guest's data.
      tags:
        - guests
      operationId: retrieveGuest
      x-eov-operation-handler: endpoints
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Guest'
        '400':
          $ref: '#/components/responses/BadRequest'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/ServerError'
    put:
      description: Modifies guest's data.
      tags:
        - guests
      operationId: replaceGuest
      x-eov-operation-handler: endpoints
      requestBody:
        description: Guest data
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Guest'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Guest'
          headers:
            Location:
              description: URI of new item.
              explode: false
              required: true
              schema:
                minLength: 1
                nullable: false
                type: string
              style: simple
        '400':
          $ref: '#/components/responses/BadRequest'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/ServerError'
    delete:
      description: Deletes a guest's data.
      tags:
        - guests
      operationId: deleteGuest
      x-eov-operation-handler: endpoints
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Guest'
          headers:
            Location:
              description: URI of new item.
              explode: false
              required: true
              schema:
                minLength: 1
                nullable: false
                type: string
              style: simple
        '400':
          $ref: '#/components/responses/BadRequest'
        '404':
          $ref: '#/components/responses/NotFound'
        '500':
          $ref: '#/components/responses/ServerError'
  /version:
    description: Get current API version number
    get:
      description: Gets current operating version of the API
      tags:
        - version
      operationId: getAPIVersion
      x-eov-operation-handler: endpoints
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/APIVersion'
components:
  responses:
    BadRequest:
      description: The request was malformed
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/EovError'
    Conflict:
      description: Conflict
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/EovError'
    NotFound:
      description: The specified resource was not found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/EovError'
    ServerError:
      description: There was an error in the server.
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/EovError'
  schemas:
    EovError:
      description: Not Acceptable
      type: object
      example:
        message: '.response should have required property ''wsuID'',
          .response should have required property ''zipCode'''
        errors:
          - path: .response.wsuID
            message: should have required property 'wsuID'
            errorCode: required.openapi.validation
          - path: .response.zipCode
            message: should have required property 'zipCode'
            errorCode: required.openapi.validation
      properties:
        message:
          type: string
        errors:
          type: array
          items:
            type: object
            properties:
              path:
                type: string
              message:
                type: string
              errorCode:
                type: string
    Guest:
      example:
        wsuID: '1234567'
        resident: true
        zipCode: "02048"
        unemployment: false
        assistance:
          socSec: false
          TANF: false
          finAid: false
          other: false
          SNAP: false
          WIC: false
          breakfast: false
          lunch: false
          SFSP: false
        guestAge: 42
        numberInHousehold: 4
      description: Guest object
      type: object
      properties:
        wsuID:
          $ref: '#/components/schemas/WSUID'
        resident:
          $ref: '#/components/schemas/Resident'
        zipCode:
          $ref: '#/components/schemas/ZipCode'
        unemployment:
          $ref: '#/components/schemas/Unemployment'
        assistance:
          $ref: '#/components/schemas/Assistance'
        guestAge:
          $ref: '#/components/schemas/GuestAge'
        numberInHousehold:
          $ref: '#/components/schemas/NumberInHousehold'
      required:
        - wsuID
        - resident
        - zipCode
        - unemployment
        - assistance
        - guestAge
        - numberInHousehold
    GuestAge:
      example: 43
      description: Age of guest in years
      type: number
    NumberInHousehold:
      example: 4
      description: Number of people living in the guest's household
      type: number
    GuestArray:
      type: array
      items:
        $ref: '#/components/schemas/Guest'
    WSUID:
      example: '1211141'
      description: Unique Worcester State University identifier
      pattern: '^[0-9]{7}$'
      type: string
    Resident:
      example: true
      description: whether the guest lives on campus or not
      type: boolean
    ZipCode:
      example: "02048"
      description: guest zip code (01602 if resident)
      pattern: '^[0-9]{5}(?:-[0-9]{4})?$'
      type: string
    Unemployment:
      example: false
      description: if the guest receives unemployment benefits
      type: boolean
    Assistance:
      example:
        socSec: false
        TANF: false
        finAid: false
        other: false
        SNAP: false
        WIC: false
        breakfast: false
        lunch: false
        SFSP: false
      description: the kind of aid that the guest receives
      type: object
      properties:
        socSec:
          type: boolean
        TANF:
          type: boolean
        finAid:
          type: boolean
        other:
          type: boolean
        SNAP:
          type: boolean
        WIC:
          type: boolean
        breakfast:
          type: boolean
        lunch:
          type: boolean
        SFSP:
          type: boolean
      required:
        - socSec
        - TANF
        - finAid
        - other
        - SNAP
        - WIC
        - breakfast
        - lunch
        - SFSP
    APIVersion:
      example: 1.2.3
      description: Current operating version of the API
      # yamllint disable-line rule:line-length
      pattern: ^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$
      type: string
